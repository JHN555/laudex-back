package com.laudex.escuela.controllers;

import com.laudex.escuela.models.dtos.ResAlumnoDTO;
import com.laudex.escuela.models.dtos.SaveAlumnoDTO;
import com.laudex.escuela.models.entities.Alumno;
import com.laudex.escuela.services.interfaces.IAlumnoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.List;

/**
 * Controller inicio
 */
@RestController
@RequestMapping("/api/alumnos")
public class AlumnoController {

    @Autowired
    private IAlumnoService alumnoService;

    /* Listado de alumnos */
    @GetMapping("")
    public List<ResAlumnoDTO> list(){
        return alumnoService.list();
    }

    /* Muestra un alumno */
    @GetMapping("/show/{id}")
    public Alumno list(@PathVariable Integer id){
        return alumnoService.show(id);
    }

    /* Guarda un alumno en la base de datos */
    @PostMapping("")
    public ResAlumnoDTO save(@RequestBody SaveAlumnoDTO saveAlumnoDTO) throws ParseException {
        return alumnoService.save(saveAlumnoDTO);
    }
}
