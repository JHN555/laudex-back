package com.laudex.escuela.auth.services;

import com.laudex.escuela.models.dtos.CustomUser;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

import javax.crypto.SecretKey;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.stream.Collectors;

@Component
public class JWTServiceImpl implements IJWTService{

    public static final SecretKey SECRET_KEY = Keys.secretKeyFor(SignatureAlgorithm.HS512);
    public static final long EXPIRATION = 14400000L;
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";

    @Override
    public String create(Authentication authentication) throws IOException {
        String username = ( (User) authentication.getPrincipal()).getUsername();

        long id = ( (CustomUser) authentication.getPrincipal()).getId();

        Collection<? extends GrantedAuthority> roles = authentication.getAuthorities();

        /*String cad = "";
        for(GrantedAuthority grantedAuthority : roles) {
            cad = cad.concat(grantedAuthority.getAuthority());
        }

        String roles1 = authentication.getAuthorities().toString();
        roles1 = roles1.replace("[", "").replace("]", "");
        */

        String authorities = authentication.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.joining(","));


        Claims claims = Jwts.claims();
        //claims.put("authorities", new ObjectMapper().writeValueAsString(roles));
        /*claims.put("authorities", roles.stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList()));*/

        claims.put("authorities", authorities); //new ObjectMapper().writeValueAsString(roles));


        claims.put("id", id);

        //SecretKey secretKey = Keys.secretKeyFor(SignatureAlgorithm.HS512);

        System.out.print("Se creo un token " + SECRET_KEY.getEncoded().toString());

        return Jwts.builder()
                .setClaims(claims)
                .setSubject(username)
                .signWith(SECRET_KEY)
                .setIssuedAt(new Date())
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRATION))
                .compact();
    }

    @Override
    public boolean validate(String token) {
        try {
            getClaims(token);
            return true;
        } catch (JwtException | IllegalArgumentException e) {
            return false;
        }
    }

    @Override
    public Claims getClaims(String token) {
        return Jwts.parserBuilder()
                .setSigningKey(SECRET_KEY)
                .build().parseClaimsJws(resolve(token))
                .getBody();
    }

    @Override
    public String getUsername(String token) {
        return getClaims(token).getSubject();
    }

    @Override
    public Collection<? extends GrantedAuthority> getRoles(String token) throws IOException {
        return Arrays.stream(getClaims(token).get("authorities").toString().split(","))
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());
    }

    @Override
    public String resolve(String token) {
        if(token != null && token.startsWith(TOKEN_PREFIX)) {
            return token.replace(TOKEN_PREFIX, "");
        }
        return null;
    }
}
