package com.laudex.escuela.models.dtos;

import lombok.Data;

/* DTO para guardar los datos de un alumno */
@Data
public class SaveAlumnoDTO {
    private Integer id;
    private String Nombre;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String fechaNacimiento;
    private String sexo;
    private String gradoEstudiosActual;
    private String email;
    private String telefono;
}
