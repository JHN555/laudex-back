package com.laudex.escuela.models.dtos;

import com.laudex.escuela.models.entities.Alumno;
import lombok.Data;

/**
 * DTO para mostrar el resumen de un alumno
 */
@Data
public class ResAlumnoDTO {

    public ResAlumnoDTO(Alumno alumno) {
        this.id = alumno.getId();
        this.nombre = alumno.getNombreCompleto();
        this.gradoEstudiosActual = alumno.getGradoEstudiosActual();
        this.telefono = alumno.getTelefono();
        this.email = alumno.getEmail();
    }

    private Integer id;
    private String nombre;
    private String gradoEstudiosActual;
    private String telefono;
    private String email;
}
