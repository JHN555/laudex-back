package com.laudex.escuela.models.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;
import java.util.Date;

@Data
@Entity
@Table(name = "alumnos")
public class Alumno {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "INT UNSIGNED")
    private Integer id;

    @Size(min = 2, max = 64)
    @Column(columnDefinition = "VARCHAR(64) NOT NULL")
    private String nombre;

    @Size(min = 2, max = 64)
    @Column(columnDefinition = "VARCHAR(64) NOT NULL")
    private String apellidoPaterno;

    @Size(min = 2, max = 64)
    @Column(columnDefinition = "VARCHAR(64) NOT NULL")
    private String apellidoMaterno;

    @JsonFormat(pattern="dd/MM/yyyy")
    @Column(columnDefinition = "DATETIME")
    private Date fechaNacimiento;

    @Size(min = 1, max = 1)
    @Column(columnDefinition = "VARCHAR(1) NOT NULL")
    private String sexo;

    @Size(min = 4, max = 64)
    @Column(columnDefinition = "VARCHAR(128)")
    private String gradoEstudiosActual;

    @Email
    @Size(min = 6, max = 128)
    @Column(unique = true, columnDefinition = "VARCHAR(128)")
    private String email;

    @Size(min = 10, max = 10)
    @Column(unique = true, columnDefinition = "VARCHAR(10)")
    private String telefono;

    public String getNombreCompleto(){
        return nombre.concat(" ").concat(apellidoPaterno).concat(" ").concat(apellidoMaterno);
    }
}
