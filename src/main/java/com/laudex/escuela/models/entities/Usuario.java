package com.laudex.escuela.models.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

@Data
@Entity
@Table(name = "usuarios")
public class Usuario {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "INT UNSIGNED")
    private Integer id;

    @Column(unique = true)
    private String username;

    @NotEmpty
    @Size(min = 2, max = 128)
    @Column(columnDefinition = "VARCHAR(128) NOT NULL")
    private String nombre;

    @NotEmpty
    @Size(min = 2, max = 128)
    @Column(columnDefinition = "VARCHAR(128) NOT NULL")
    private String apellidoPaterno;

    @NotEmpty
    @Size(min = 2, max = 128)
    @Column(columnDefinition = "VARCHAR(128) NOT NULL")
    private String apellidoMaterno;

    @Email
    @Size(min = 2, max = 128)
    @Column(unique = true, columnDefinition = "VARCHAR(128)")
    private String email;

    @NotEmpty
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Size(min = 5, max = 128)
    @Column(columnDefinition = "VARCHAR(128) NOT NULL ")
    private String password;

    private boolean enabled;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<Rol> roles;

    @JsonIgnore
    @Column(columnDefinition = "VARCHAR(2) NOT NULL DEFAULT 'A'")
    private String estatus;

    @JsonIgnore
    @Column(columnDefinition = "DATETIME DEFAULT CURRENT_TIMESTAMP")
    @CreationTimestamp
    private Date createdAt;

    @JsonIgnore
    @Column(columnDefinition = "DATETIME  DEFAULT CURRENT_TIMESTAMP")
    @UpdateTimestamp
    private Date updatedAt;

    public String getNombreCompleto(){
        return nombre.concat(" ").concat(apellidoPaterno).concat(" ").concat(apellidoMaterno);
    }
}
