package com.laudex.escuela.models.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "roles")
public class Rol {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(unique = true, columnDefinition = "varchar (255) default ''")
    private String authority;

    @Column(unique = true, columnDefinition = "varchar (255) default ''")
    private String descripcion;

    @JsonIgnore
    @Column(columnDefinition = "VARCHAR(2) NOT NULL DEFAULT 'A'")
    private String estatus;

    @JsonIgnore
    @Column(columnDefinition = "DATETIME DEFAULT CURRENT_TIMESTAMP")
    @CreationTimestamp
    private Date createdAt;

    @JsonIgnore
    @Column(columnDefinition = "DATETIME  DEFAULT CURRENT_TIMESTAMP")
    @UpdateTimestamp
    private Date updatedAt;
}
