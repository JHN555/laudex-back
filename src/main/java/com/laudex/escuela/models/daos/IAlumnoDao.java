package com.laudex.escuela.models.daos;

import com.laudex.escuela.models.entities.Alumno;
import org.springframework.data.jpa.repository.JpaRepository;

/* Acceso a datos para la entity Alumno */
public interface IAlumnoDao extends JpaRepository<Alumno, Integer> {
}
