package com.laudex.escuela.models.daos;

import com.laudex.escuela.models.entities.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

/* Acceso a datos para la entity Usuario */
public interface IUsuarioDao extends JpaRepository<Usuario, Integer> {
    Usuario findOneByEmail(@Param("email") String email);
}
