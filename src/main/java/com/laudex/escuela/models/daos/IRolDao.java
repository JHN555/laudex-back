package com.laudex.escuela.models.daos;

import com.laudex.escuela.models.entities.Rol;
import org.springframework.data.jpa.repository.JpaRepository;

/* Acceso a datos para la entity Rol */
public interface IRolDao extends JpaRepository<Rol, Integer> {
}
