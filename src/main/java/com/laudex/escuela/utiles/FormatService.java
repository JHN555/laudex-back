package com.laudex.escuela.utiles;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/* Esta clase nos permite realizar la conversion de fechas de tipo string a Date y viceversa */
public class FormatService {
    public static String FORMATO_FRONT = "dd/MM/yyyy";
    public static String FORMATO_DATE = "yyyy-MM-dd";

    public static String dateToString(Date fecha, String formato){
        if(fecha == null)
            return "";

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(formato);
        return simpleDateFormat.format(fecha);
    }

    public static Date stringToDate(String fecha, String formato) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat(formato);
        return sdf.parse(fecha);
    }
}
