package com.laudex.escuela.services;

import com.laudex.escuela.models.daos.IAlumnoDao;
import com.laudex.escuela.models.dtos.ResAlumnoDTO;
import com.laudex.escuela.models.dtos.SaveAlumnoDTO;
import com.laudex.escuela.models.entities.Alumno;
import com.laudex.escuela.services.interfaces.IAlumnoService;
import com.laudex.escuela.utiles.FormatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/* Servicio con los metodos para listar, mostrar y guardar usuarios */
@Service
public class AlumnoServiceImpl implements IAlumnoService {

    @Autowired
    private IAlumnoDao alumnoDao;

    @Override
    public List<ResAlumnoDTO> list() {
        List<Alumno> alumnoList = alumnoDao.findAll();
        List<ResAlumnoDTO> resAlumnoDTOList = new ArrayList<>();

        for(Alumno alumno : alumnoList){
            resAlumnoDTOList.add(new ResAlumnoDTO(alumno));
        }

        return resAlumnoDTOList;
    }

    @Override
    public Alumno show(Integer id) {
        return alumnoDao.findById(id).orElseThrow();
    }

    @Transactional
    @Override
    public ResAlumnoDTO save(SaveAlumnoDTO saveAlumnoDTO) throws ParseException {

        Alumno alumno = new Alumno();
        alumno.setNombre(saveAlumnoDTO.getNombre());
        alumno.setApellidoPaterno(saveAlumnoDTO.getApellidoPaterno());
        alumno.setApellidoMaterno(saveAlumnoDTO.getApellidoMaterno());
        alumno.setFechaNacimiento(FormatService.stringToDate(saveAlumnoDTO.getFechaNacimiento(), FormatService.FORMATO_DATE));
        alumno.setSexo(saveAlumnoDTO.getSexo());
        alumno.setGradoEstudiosActual(saveAlumnoDTO.getGradoEstudiosActual());
        alumno.setEmail(saveAlumnoDTO.getEmail());
        alumno.setTelefono(saveAlumnoDTO.getTelefono());

        alumnoDao.save(alumno);
        return new ResAlumnoDTO(alumno);
    }
}
