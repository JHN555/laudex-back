package com.laudex.escuela.services;

import com.laudex.escuela.models.daos.IUsuarioDao;
import com.laudex.escuela.models.entities.Usuario;
import com.laudex.escuela.services.interfaces.IUsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/* Servicio para el control de usuarios */
@Service
public class UsuarioServiceImpl implements IUsuarioService {
    @Autowired
    private IUsuarioDao usuarioDao;

    @Override
    public Usuario show(Integer id) {
        return usuarioDao.getById(id);
    }
}
