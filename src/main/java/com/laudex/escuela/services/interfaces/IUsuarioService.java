package com.laudex.escuela.services.interfaces;

import com.laudex.escuela.models.entities.Usuario;

/* Interface para la parte usuarios mediante el patron de diseño de fachada */
public interface IUsuarioService {
    Usuario show(Integer id);
}
