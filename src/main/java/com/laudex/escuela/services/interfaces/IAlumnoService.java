package com.laudex.escuela.services.interfaces;

import com.laudex.escuela.models.dtos.ResAlumnoDTO;
import com.laudex.escuela.models.dtos.SaveAlumnoDTO;

import com.laudex.escuela.models.entities.Alumno;

import java.text.ParseException;
import java.util.List;

/* Interface para la parte de alumnos mediante el patron de diseño de fachada */
public interface IAlumnoService {

    List<ResAlumnoDTO> list();
    Alumno show(Integer id);
    ResAlumnoDTO save(SaveAlumnoDTO saveAlumnoDTO) throws ParseException;
}
