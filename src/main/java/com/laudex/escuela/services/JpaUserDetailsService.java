package com.laudex.escuela.services;

import com.laudex.escuela.models.daos.IUsuarioDao;
import com.laudex.escuela.models.dtos.CustomUser;
import com.laudex.escuela.models.entities.Rol;
import com.laudex.escuela.models.entities.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/* Servicio para la gestion del usuario que ingresa al sistema */
@Service
public class JpaUserDetailsService implements UserDetailsService {

    @Autowired
    private IUsuarioDao usuarioDao;

    /* Se busca al usuario en la base de datos  */
    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Usuario usuario = usuarioDao.findOneByEmail(s);
        List<GrantedAuthority> authorities = new ArrayList<>();

        /* Se valida que exista el usuario y se asignan los roles*/
        if(usuario != null) {
            for(Rol role: usuario.getRoles()) {
                authorities.add(new SimpleGrantedAuthority("ROLE_" + role.getAuthority()));
                System.out.println("Rol =  " + role.getAuthority());
            }
        }else {
            System.out.println("Error no existe el usuario");
        }

        /* Es requerido que el usuario cuente con al menos un rol */
        if(authorities.isEmpty()){
            throw new UsernameNotFoundException("El usuario no tiene roles asignados");
        }

        return new CustomUser(usuario.getUsername(), usuario.getPassword(), usuario.isEnabled(), true,
                true, true, authorities, usuario.getId());
    }
}
