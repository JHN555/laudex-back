-- Se insertar los roles de usuario
INSERT INTO roles (authority, descripcion) VALUES ("admin", "admin");

-- Se insertan usuarios de prueba
INSERT INTO usuarios (username, nombre, apellido_paterno, apellido_materno, email, password, enabled) VALUES ('Johan', 'Johan', 'Contreras', 'Camargo', 'johan@laudex.com', '$2y$12$2oS.MeOw8D32SyVnDesReuYWyDw6b6HpSUFoO5nK8J/e7DrCj9hiO', 1);
INSERT INTO usuarios (username, nombre, apellido_paterno, apellido_materno, email, password, enabled) VALUES ('Miguel', 'Miguel', 'Sanchez', '', 'miguel@laudex.com', '$2y$12$2oS.MeOw8D32SyVnDesReuYWyDw6b6HpSUFoO5nK8J/e7DrCj9hiO', 1);
INSERT INTO usuarios (username, nombre, apellido_paterno, apellido_materno, email, password, enabled) VALUES ('Araceli', 'Araceli', 'Guerrero', '', 'araceli@laudex.com', '$2y$12$2oS.MeOw8D32SyVnDesReuYWyDw6b6HpSUFoO5nK8J/e7DrCj9hiO', 1);

-- Aqui se insertan los roles de usuarios
INSERT INTO usuarios_roles (usuario_id, roles_id) VALUES (1,1);
INSERT INTO usuarios_roles (usuario_id, roles_id) VALUES (2,1);
INSERT INTO usuarios_roles (usuario_id, roles_id) VALUES (3,2);

-- Se insertan algunos alumnos de prueba
INSERT INTO alumnos (nombre, apellido_paterno, apellido_materno, email, fecha_nacimiento, grado_estudios_actual, sexo, telefono) VALUES ('Luis', 'Lara', 'Linares', 'luis@gmail.com', '1993-07-06', 'Ingenieria', 'H', '551234578');
INSERT INTO alumnos (nombre, apellido_paterno, apellido_materno, email, fecha_nacimiento, grado_estudios_actual, sexo, telefono) VALUES ('Alberto', 'Alanis', 'Azcona', 'alberto@hotmail.com', '2005-02-22', 'Secundaria', 'H', '553219584');
INSERT INTO alumnos (nombre, apellido_paterno, apellido_materno, email, fecha_nacimiento, grado_estudios_actual, sexo, telefono) VALUES ('Daniela', 'Dorantes', 'Davalos', 'daniela@outlook.com', '1999-08-13', 'Bachillerato', 'M', '557457896');
INSERT INTO alumnos (nombre, apellido_paterno, apellido_materno, email, fecha_nacimiento, grado_estudios_actual, sexo, telefono) VALUES ('Carlos', 'Carrete', 'Corona', 'carlos@mail.com', '2007-02-19', 'Bachillerato', 'H', '558526597');
INSERT INTO alumnos (nombre, apellido_paterno, apellido_materno, email, fecha_nacimiento, grado_estudios_actual, sexo, telefono) VALUES ('Sofia', 'Sanchez', 'Serrano', 'sofia@live.com', '2010-04-11', 'Secundaria', 'M', '559348959');

