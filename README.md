Para el correr el proyecto es necesario establecer las credenciales del motor de base de datos en el archivo
application.properties de spring 

Ejemplo:
spring.datasource.username=laudex
spring.datasource.password=clavelaudex

Se puede configurar esta linea 
spring.jpa.hibernate.ddl-auto=none 
para que cree automaticamente la base de datos, estableciendo la propiedad a "create"

De igual forma se puede importar el archivo .sql que se encuentra en: resources/static/db_laudex.sql
